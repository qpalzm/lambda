package com.company;

import com.company.process.Process;

public class Main {

    public static void main(String[] args) {
        Process p = new Process();
        p.runStream();
        p.runOptional();
    }
}
