package com.company.data;

public class MyDto {

    private Boolean fieldBoolean;

    private Integer fieldInteger;


    public Boolean getFieldBoolean() {
        return fieldBoolean;
    }

    public void setFieldBoolean(Boolean fieldBoolean) {
        this.fieldBoolean = fieldBoolean;
    }

    public Integer getFieldInteger() {
        return fieldInteger;
    }

    public void setFieldInteger(Integer fieldInteger) {
        this.fieldInteger = fieldInteger;
    }

    @Override
    public String toString() {
        return "MyDto{" +
                "fieldBoolean=" + fieldBoolean +
                ", fieldInteger=" + fieldInteger +
                '}';
    }
}
