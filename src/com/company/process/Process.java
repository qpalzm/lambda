package com.company.process;

import com.company.data.MyDto;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class Process {

    private static final int SIZE_LIST = 10;
    private static final int MIN = 0;
    private static final int MAX = 1000;

    public List<MyDto> createListDto() {
        Random r = new Random();
        List<MyDto> list = new ArrayList<>();
        for (int i = 0; i < SIZE_LIST; i++) {
            MyDto dto = new MyDto();
            int nextInt = r.nextInt((MAX - MIN) + 1);
            dto.setFieldInteger(nextInt);
            dto.setFieldBoolean(nextInt % 2 == 0);
            list.add(dto);
        }
        return list;
    }

    public void runStream(){
        List<MyDto> listDto = createListDto();
        listDto.forEach(System.out::println);
        MyDto maxDto = listDto.stream().max((p1, p2) -> p1.getFieldInteger().compareTo(p2.getFieldInteger())).get();
        System.out.println("Max dto " + maxDto);
    }

    public void runOptional(){
        System.out.println("runOptional()");
        List<MyDto> listDto = createListDto();
        List<Optional<String>> optionalList = new ArrayList<>();
        List<String> list = new ArrayList<>();
        listDto.forEach(System.out::println);
        listDto.forEach(d ->{
            if (d.getFieldBoolean()){
                optionalList.add(Optional.of(d.getFieldInteger().toString()));
                list.add(null);
            } else {
                optionalList.add(Optional.empty());
                list.add(d.getFieldInteger().toString());
            }
        });

        optionalList.forEach(o ->{
            o.ifPresent(System.out::println);
        });
        System.out.println("==================");
        list.stream().filter(Objects::nonNull).forEach(System.out::println);
        System.out.println("==================");
        List<String> collect = optionalList.stream().filter(Optional::isPresent).map(Optional::get)
                .collect(Collectors.toList());
        collect.forEach(System.out::println);
    }

}
